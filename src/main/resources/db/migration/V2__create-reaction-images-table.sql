create table reaction_images
(
    reaction_identifier text not null primary key,
    emoji_url           text null,
    emoji_unicode       text null,

    check (emoji_url is not null or emoji_unicode is not null)
);