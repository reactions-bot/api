alter table reactions
    drop column channel_id,
    drop column author_user_id,
    drop column target_user_id;

alter table reactions
    add column channel_id bigint not null default 0,
    add column author_user_id bigint not null default 0,
    add column target_user_id bigint not null default 0;
