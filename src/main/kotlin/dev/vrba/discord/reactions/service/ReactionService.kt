package dev.vrba.discord.reactions.service

import dev.vrba.discord.reactions.domain.DailyReactionReport
import dev.vrba.discord.reactions.entity.Reaction
import dev.vrba.discord.reactions.entity.ReactionImage
import dev.vrba.discord.reactions.repository.ReactionImageRepository
import dev.vrba.discord.reactions.repository.ReactionRepository
import jakarta.inject.Singleton
import kotlinx.coroutines.flow.toList
import java.lang.IllegalArgumentException
import java.math.BigInteger
import java.time.Clock
import java.time.Instant
import java.time.LocalDate

@Singleton
class ReactionService(
    private val clock: Clock,
    private val reactionRepository: ReactionRepository,
    private val reactionImageRepository: ReactionImageRepository
) {

    suspend fun recordSubmittedReaction(
        channel: BigInteger,
        reaction: String,
        reactionEmojiUrl: String?,
        reactionEmojiUnicode: String?,
        reactionAuthorId: BigInteger,
        reactionTargetId: BigInteger
    ) {
        recordReaction(reaction, channel, reactionAuthorId, reactionTargetId)
        updateReactionImage(reaction, reactionEmojiUrl, reactionEmojiUnicode)
    }

    suspend fun generateDailyReactionReport(channel: BigInteger, reaction: String, date: LocalDate): DailyReactionReport {
        val start = date.atStartOfDay(clock.zone).toInstant()
        val end = date.plusDays(1).atStartOfDay(clock.zone).toInstant()

        val reactions = reactionRepository.findAllByReactionAndChannelAndTimestampBetween(reaction, channel, start, end).toList()
        val image = reactionImageRepository.findById(reaction) ?: ReactionImage(reaction)

        return DailyReactionReport(
            image = image,
            totalReactions = reactions.size,
            reactionCounts = reactions.groupBy { it.author }.mapValues { it.value.size },
            reactionCountsMatrix = reactions.groupBy { it.author }.mapValues { author ->
                author.value.groupBy { it.target }.mapValues { it.value.size }
            }
        )
    }

    private suspend fun recordReaction(reaction: String, channel: BigInteger, author: BigInteger, target: BigInteger) {
        val entity = Reaction(
            reaction = reaction,
            channel = channel,
            author = author,
            target = target,
            timestamp = Instant.now(clock)
        )

        println(entity)

        reactionRepository.save(entity)
    }

    private suspend fun updateReactionImage(identifier: String, url: String?, unicode: String?) {
        val reaction = reactionImageRepository.findById(identifier)

        if (reaction == null) {
            reactionImageRepository.save(
                ReactionImage(
                    id = identifier,
                    url = url,
                    unicode = unicode
                )
            )
            return
        }

        if (reaction.url != url || reaction.unicode != unicode) {
            val updated = reaction.copy(
                id = identifier,
                url = url,
                unicode = unicode
            )

            reactionImageRepository.update(updated)
        }
    }
}