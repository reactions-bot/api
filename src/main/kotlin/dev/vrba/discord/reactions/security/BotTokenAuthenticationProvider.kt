package dev.vrba.discord.reactions.security

import dev.vrba.discord.reactions.configuration.BotTokenConfiguration
import io.micronaut.http.HttpRequest
import io.micronaut.security.authentication.AuthenticationProvider
import io.micronaut.security.authentication.AuthenticationRequest
import io.micronaut.security.authentication.AuthenticationResponse
import jakarta.inject.Singleton
import org.reactivestreams.Publisher
import reactor.core.publisher.Flux

@Singleton
class BotTokenAuthenticationProvider(private val configuration: BotTokenConfiguration) : AuthenticationProvider<HttpRequest<*>> {

    override fun authenticate(request: HttpRequest<*>?, authentication: AuthenticationRequest<*, *>): Publisher<AuthenticationResponse> {
        return Flux.create {
            if (
                authentication.identity == configuration.username &&
                authentication.secret == configuration.token
            ) {
                val roles = listOf("ROLE_BOT")
                val response = AuthenticationResponse.success(configuration.username, roles)

                it.next(response)
            }
            else {
                it.next(AuthenticationResponse.failure())
            }
        }
    }

}