package dev.vrba.discord.reactions.domain

import dev.vrba.discord.reactions.entity.ReactionImage
import java.math.BigInteger

typealias UserId = BigInteger

data class DailyReactionReport(
    val totalReactions: Int,
    val reactionCounts: Map<UserId, Int>,
    val reactionCountsMatrix: Map<UserId, Map<UserId, Int>>,
    val image: ReactionImage
)