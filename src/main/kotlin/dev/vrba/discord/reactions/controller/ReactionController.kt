package dev.vrba.discord.reactions.controller

import dev.vrba.discord.reactions.request.ReactionSubmissionRequest
import dev.vrba.discord.reactions.response.DailyReactionsReportResponse
import dev.vrba.discord.reactions.response.dto.toDto
import dev.vrba.discord.reactions.service.ReactionService
import io.micronaut.core.convert.format.Format
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import io.micronaut.validation.Validated
import jakarta.validation.Valid
import java.math.BigInteger
import java.time.LocalDate

@Controller("/api/v1/reactions")
open class ReactionController(private val service: ReactionService) {

    @Post("/submit")
    @Secured("ROLE_BOT")
    open suspend fun submit(@Valid @Body request: ReactionSubmissionRequest): HttpResponse<Unit> {
        service.recordSubmittedReaction(
            request.channel,
            request.reaction,
            request.reactionEmojiUrl,
            request.reactionEmojiUnicode,
            request.reactionAuthorId,
            request.reactionTargetId,
        )

        return HttpResponse.accepted()
    }

    @Get("/daily-report/{channel}/{reaction}/{date}")
    @Secured(SecurityRule.IS_ANONYMOUS)
    open suspend fun dailyReactionReport(
        @QueryValue channel: BigInteger,
        @QueryValue reaction: String,
        @QueryValue @Format("yyyy-MM-dd") date: LocalDate
    ): HttpResponse<DailyReactionsReportResponse> {
        val report = service.generateDailyReactionReport(channel, reaction, date)
        val response = DailyReactionsReportResponse(report.toDto())

        return HttpResponse.ok(response)
    }
}