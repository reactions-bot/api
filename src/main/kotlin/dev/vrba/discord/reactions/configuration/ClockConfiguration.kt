package dev.vrba.discord.reactions.configuration

import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import java.time.Clock
import java.time.ZoneId

@Factory
class ClockConfiguration {

    @Bean
    fun clock(): Clock {
        return Clock.system(ZoneId.of("Europe/Prague"))
    }

}