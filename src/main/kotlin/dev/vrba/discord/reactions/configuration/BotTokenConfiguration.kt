package dev.vrba.discord.reactions.configuration

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("bot-token")
class BotTokenConfiguration {
    lateinit var username: String
    lateinit var token: String
}