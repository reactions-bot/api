package dev.vrba.discord.reactions.request

import io.micronaut.serde.annotation.Serdeable
import java.math.BigInteger

@Serdeable
data class ReactionSubmissionRequest(
    val channel: BigInteger,
    val reaction: String,
    val reactionEmojiUrl: String? = null,
    val reactionEmojiUnicode: String? = null,
    val reactionAuthorId: BigInteger,
    val reactionTargetId: BigInteger,
)