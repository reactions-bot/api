package dev.vrba.discord.reactions.repository

import dev.vrba.discord.reactions.entity.ReactionImage
import io.micronaut.cache.annotation.CacheInvalidate
import io.micronaut.cache.annotation.CachePut
import io.micronaut.cache.annotation.Cacheable
import io.micronaut.data.annotation.Repository
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.r2dbc.annotation.R2dbcRepository
import io.micronaut.data.repository.jpa.kotlin.CoroutineJpaSpecificationExecutor
import io.micronaut.data.repository.kotlin.CoroutineCrudRepository

@Repository
@R2dbcRepository(dialect = Dialect.POSTGRES)
interface ReactionImageRepository : CoroutineCrudRepository<ReactionImage, String>, CoroutineJpaSpecificationExecutor<ReactionImage> {

    @Cacheable("reactionImages", parameters = ["id"])
    override suspend fun findById(id: String): ReactionImage?

    @CachePut("reactionImages", parameters = ["entity#id"])
    override suspend fun <S : ReactionImage> update(entity: S): S
}