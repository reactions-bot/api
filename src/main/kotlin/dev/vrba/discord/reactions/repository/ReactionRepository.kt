package dev.vrba.discord.reactions.repository

import dev.vrba.discord.reactions.entity.Reaction
import io.micronaut.data.annotation.Repository
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.r2dbc.annotation.R2dbcRepository
import io.micronaut.data.repository.jpa.kotlin.CoroutineJpaSpecificationExecutor
import io.micronaut.data.repository.kotlin.CoroutineCrudRepository
import kotlinx.coroutines.flow.Flow
import java.math.BigInteger
import java.time.Instant

@Repository
@R2dbcRepository(dialect = Dialect.POSTGRES)
interface ReactionRepository : CoroutineCrudRepository<Reaction, Long>, CoroutineJpaSpecificationExecutor<Reaction> {

    fun findAllByReactionAndChannelAndTimestampBetween(reaction: String, channel: BigInteger, start: Instant, end: Instant): Flow<Reaction>

}
