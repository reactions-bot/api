package dev.vrba.discord.reactions.response

import dev.vrba.discord.reactions.response.dto.DailyReactionReportDto
import io.micronaut.serde.annotation.Serdeable

@Serdeable
data class DailyReactionsReportResponse(
    val report: DailyReactionReportDto
)