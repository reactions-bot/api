package dev.vrba.discord.reactions.response

import io.micronaut.serde.annotation.Serdeable
import java.time.Instant

@Serdeable
data class ReactionSubmissionResponse(
    val timestamp: Instant
)
