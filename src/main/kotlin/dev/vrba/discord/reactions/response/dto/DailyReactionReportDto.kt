package dev.vrba.discord.reactions.response.dto

import dev.vrba.discord.reactions.domain.DailyReactionReport
import io.micronaut.serde.annotation.Serdeable
import java.math.BigInteger

@Serdeable
data class DailyReactionReportDto(
    val totalReactions: Int,
    val reactionCounts: Map<String, Int>,
    val reactionCountsMatrix: Map<String, Map<String, Int>>,
    val image: ReactionImageDto,
)

fun <V> Map<BigInteger, V>.keysToString(): Map<String, V> {
    return this.mapKeys { it.key.toString() }
}

fun DailyReactionReport.toDto() = DailyReactionReportDto(
    totalReactions,
    reactionCounts.keysToString(),
    reactionCountsMatrix.keysToString().mapValues { it.value.keysToString() },
    image.toDto()
)