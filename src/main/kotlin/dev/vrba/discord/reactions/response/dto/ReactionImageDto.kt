package dev.vrba.discord.reactions.response.dto

import dev.vrba.discord.reactions.entity.ReactionImage
import io.micronaut.serde.annotation.Serdeable

@Serdeable
data class ReactionImageDto(
    val url: String?,
    val unicode: String?
)

fun ReactionImage.toDto() = ReactionImageDto(url, unicode)