package dev.vrba.discord.reactions.entity

import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import io.micronaut.data.annotation.MappedProperty
import java.math.BigInteger
import java.time.Instant

@MappedEntity("reactions")
data class Reaction(
    @Id
    @GeneratedValue(GeneratedValue.Type.IDENTITY)
    @MappedProperty("id")
    val id: Long? = null,

    @MappedProperty("channel_id")
    val channel: BigInteger,

    @MappedProperty("author_user_id")
    val author: BigInteger,

    @MappedProperty("target_user_id")
    val target: BigInteger,

    @MappedProperty("reaction_identifier")
    val reaction: String,

    @MappedProperty("timestamp")
    val timestamp: Instant = Instant.now(),
)
