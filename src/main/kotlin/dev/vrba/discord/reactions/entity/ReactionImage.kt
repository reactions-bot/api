package dev.vrba.discord.reactions.entity

import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import io.micronaut.data.annotation.MappedProperty

@MappedEntity("reaction_images")
data class ReactionImage(
    @Id
    @MappedProperty("reaction_identifier")
    val id: String,

    @MappedProperty("emoji_url")
    val url: String? = null,

    @MappedProperty("emoji_unicode")
    val unicode: String? = null
)